<?php

/**
 * @noinspection PhpDocSignatureInspection
 * @noinspection PhpIncludeInspection
 */

namespace Zalmoksis\Dictionary\Parser\Yaml\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Parser\Yaml\{
    PhpYamlParser,
    YamlSerializer,
};
use Zalmoksis\Dictionary\Parser\ArrayParser\{
    DefaultArraySerializer,
    DefaultArraySerializerConfiguration,
    DefaultArrayStructure,
};

final class YamlSerializerTest extends TestCase {

    function provideEntriesForSerialization(): array {
        $basicEntry                  = __DIR__ . '/../samples/model/entry.php';
        $entryWithPossibleShorthands = __DIR__ . '/../samples/model/entryWithPossibleShorthands.php';

        return [
            'basic_entry' => [
                'configuration' => null,
                'yaml_file' => __DIR__ . '/../samples/yaml/entry.yaml',
                'model_file' => $basicEntry,
            ],
            'basic_entry_with_shorthands_enabled' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->allowAllShorthands(),
                'yaml_file' => __DIR__ . '/../samples/yaml/entry.yaml',
                'model_file' => $basicEntry,
            ],
            'entry_with_shorthands_when_NOT_enabled' => [
                'configuration' => null,
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsNotApplied.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_all_enabled' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->allowAllShorthands(),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithAllShorthandsApplied.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_headwords' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('headwords'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForHeadwords.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_pronunciations' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('pronunciations'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForPronunciations.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_categories' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('categories'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForCategories.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_forms' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('forms'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForForms.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_translations' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('translations'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForTranslations.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_synonyms' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('synonyms'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForSynonyms.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_antonyms' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('antonyms'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForAntonyms.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_derivatives' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands('derivatives'),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithShorthandsAppliedForDerivatives.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
            'entry_with_shorthands_when_enabled_for_each_one' => [
                'configuration' => (new DefaultArraySerializerConfiguration())
                    ->setAllowedShorthands(
                        'headwords',
                        'pronunciations',
                        'categories',
                        'forms',
                        'translations',
                        'synonyms',
                        'antonyms',
                        'derivatives'
                    ),
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithAllShorthandsApplied.yaml',
                'model_file' => $entryWithPossibleShorthands,
            ],
        ];
    }

    /**
     * @dataProvider provideEntriesForSerialization
     */
    function testSerializingEntry(
        ?DefaultArraySerializerConfiguration $configuration,
        string $yamlFile,
        string $modelFile
    ): void {
        $serializer = new YamlSerializer(
            new PhpYamlParser(),
            new DefaultArraySerializer(
                new DefaultArrayStructure(),
                $configuration,
            ));

        $this->assertStringEqualsFile(
            $yamlFile,
            $serializer->serializeEntry(require $modelFile)
        );
    }

    public function provideSensesForSerialization(): array {
        return [
            'basic_sense' => [
                'configuration' => null,
                'yaml_file' => __DIR__ . '/../samples/yaml/sense.yaml',
                'model_file' => __DIR__ . '/../samples/model/sense.php',
            ]
        ];
    }

    /**
     * @dataProvider provideSensesForSerialization
     */
    function testSerializingSense(
        ?DefaultArraySerializerConfiguration $configuration,
        string $yamlFile,
        string $modelFile
    ): void {
        $serializer = new YamlSerializer(
            new PhpYamlParser(),
            new DefaultArraySerializer(
                new DefaultArrayStructure(),
                $configuration,
            ),
        );

        $this->assertStringEqualsFile(
            $yamlFile,
            $serializer->serializeSense(require $modelFile)
        );
    }

    public function provideCollocationsForSerialization(): array {
        return [
            'basic_collocation' => [
                'configuration' => null,
                'yaml_file' => __DIR__ . '/../samples/yaml/collocation.yaml',
                'model_file' => __DIR__ . '/../samples/model/collocation.php',
            ]
        ];
    }

    /**
     * @dataProvider provideCollocationsForSerialization
     */
    function testSerializingCollocation(
        ?DefaultArraySerializerConfiguration $configuration,
        string $yamlFile,
        string $modelFile
    ): void {
        $serializer = new YamlSerializer(
            new PhpYamlParser(),
            new DefaultArraySerializer(
                new DefaultArrayStructure(),
                $configuration,
            ),
        );

        $this->assertStringEqualsFile(
            $yamlFile,
            $serializer->serializeCollocation(require $modelFile)
        );
    }
}
