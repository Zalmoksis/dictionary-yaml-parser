<?php

/**
 * @noinspection PhpDocSignatureInspection
 * @noinspection PhpIncludeInspection
 */

namespace Zalmoksis\Dictionary\Parser\Yaml\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Parser\Yaml\{
    PhpYamlParser,
    YamlDeserializer,
    YamlParsingException,
};
use Zalmoksis\Dictionary\Parser\ArrayParser\{
    DefaultArrayDeserializer,
    DefaultArrayNormalizer,
    DefaultArrayStructure,
};

final class YamlDeserializerTest extends TestCase {

    function provideEntriesForDeserialization(): array {
        return [
            'minimal entry' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/minimalEntry.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/minimalEntry.yaml',
            ],
            'basic_entry' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/entry.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/entry.yaml',
            ],
            'entry_with_all_shorthands_applied' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/entryWithPossibleShorthands.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithAllShorthandsApplied.yaml',
            ],
            'minimal entry with normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/minimalEntry.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/minimalEntry.yaml',
            ],
            'basic_entry_with_normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/minimalEntry.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/minimalEntry.yaml',
            ],
            'entry_with_all_shorthands_applied_and_normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/entryWithPossibleShorthands.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithAllShorthandsApplied.yaml',
            ],
            'entry_with_aliases_and_normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/entryWithAliasesResolved.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/entryWithAliases.yaml',
            ],
        ];
    }

    /**
     * @dataProvider provideEntriesForDeserialization
     */
    function testDeserializingEntry(
        YamlDeserializer $deserializer,
        string $modelFile,
        string $yamlFile
    ): void {
        $this->assertEquals(
            require $modelFile,
            $deserializer->deserializeEntry(
                file_get_contents($yamlFile)
            )
        );
    }

    function provideSensesForDeserialization(): array {
        return [
            'basic_sense' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                ),
                'model_file' => __DIR__ . '/../samples/model/sense.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/sense.yaml',
            ],
            'basic_sense_with_normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure())
                ),
                'model_file' => __DIR__ . '/../samples/model/sense.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/sense.yaml',
            ],
        ];
    }

    /**
     * @dataProvider provideSensesForDeserialization
     */
    function testDeserializingSense(
        YamlDeserializer $deserializer,
        string $modelFile,
        string $yamlFile
    ): void {
        $this->assertEquals(
            require $modelFile,
            $deserializer->deserializeSense(
                file_get_contents($yamlFile)
            )
        );
    }

    function provideCollocationsForDeserialization(): array {
        return [
            'basic_collocation' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure())
                ),
                'model_file' => __DIR__ . '/../samples/model/collocation.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/collocation.yaml',
            ],
            'basic_collocation_with_normalization' => [
                'deserializer' => new YamlDeserializer(
                    new PhpYamlParser(),
                    new DefaultArrayDeserializer(new DefaultArrayStructure()),
                    new DefaultArrayNormalizer(new DefaultArrayStructure())
                ),
                'model_file' => __DIR__ . '/../samples/model/collocation.php',
                'yaml_file' => __DIR__ . '/../samples/yaml/collocation.yaml',
            ],
        ];
    }

    /**
     * @dataProvider provideCollocationsForDeserialization
     */
    function testDeserializingCollocation(
        YamlDeserializer $deserializer,
        string $modelFile,
        string $yamlFile
    ): void {
        $this->assertEquals(
            require $modelFile,
            $deserializer->deserializeCollocation(
                file_get_contents($yamlFile)
            )
        );
    }

    function testParsingIncorrectYaml(): void {
        $this->expectException(YamlParsingException::class);

        (new YamlDeserializer(
            new PhpYamlParser(),
            new DefaultArrayDeserializer(new DefaultArrayStructure()),
        ))->deserializeEntry("- a\na");
    }
}
