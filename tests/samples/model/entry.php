<?php

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Collocation,
    Definition,
    Derivative,
    Entry,
    Form,
    FormGroup,
    FormLabel,
    Headword,
    HomographIndex,
    Pronunciation,
    Sense,
    Synonym,
    Translation,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Collocations,
    Derivatives,
    FormNodes,
    Headwords,
    Pronunciations,
    Senses,
    Synonyms,
    Translations,
};

return (new Entry())
    ->setHeadwords(new Headwords(
        new Headword('headword 1'),
        new Headword('ˈhɛdˌwɜːd 2'),
    ))
    ->setHomographIndex(new HomographIndex(3))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation 1'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən 2'),
    ))
    ->setCategories(new Categories(
        new Category('category 1'),
        new Category('ˈkætɪɡərɪ 2'),
    ))
    ->setFormNodes(new FormNodes(
        (new Form(new FormLabel('form label 1')))
            ->setHeadwords(new Headwords(
                new Headword('form 1.1'),
                new Headword('fɔːm 1.2'),
            )),
        (new FormGroup(new FormLabel('form label 2')))
            ->setFormNodes(new FormNodes(
                (new Form(new FormLabel('fɔːm ˈleɪ bəl 2.1')))
                    ->setHeadwords(new Headwords(
                        new Headword('form 2.1.1'),
                        new Headword('fɔːm 2.1.2'),
                    ))
            )),
    ))
    ->setDefinition(new Definition('definition 0'))
    ->setTranslations(new Translations(
        new Translation('translation 0.1'),
        new Translation('trænsˈleɪʃən 0.2'),
    ))
    ->setSynonyms(new Synonyms(
        new Synonym('synonym 1'),
        new Synonym('synonym 2'),
    ))
    ->setAntonyms(new Antonyms(
        new Antonym('antonym 1'),
        new Antonym('antonym 2'),
    ))
    ->setCollocations(new Collocations(
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword 0.a.1'),
                new Headword('ˈhɛdˌwɜːd 0.a.2'),
            ))
            ->setTranslations(new Translations(
                new Translation('translation 0.a.1'),
                new Translation('trænsˈleɪʃən 0.a.2'),
            ))
            ->setSenses(new Senses(
                (new Sense())
                    ->setTranslations(new Translations(
                        new Translation('translation 0.a.1.1'),
                        new Translation('trænsˈleɪʃən 0.a.1.2'),
                    ))
            )),
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword 0.b.1'),
                new Headword('ˈhɛdˌwɜːd 0.b.2'),
            ))
            ->setTranslations(new Translations(
                new Translation('translation 0.b.1'),
                new Translation('trænsˈleɪʃən 0.b.2'),
            )),
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setDefinition(new Definition('definition 1'))
            ->setTranslations(new Translations(
                new Translation('translation 1.1'),
                new Translation('trænsˈleɪʃən 1.2'),
            ))
            ->setCollocations(new Collocations(
                (new Collocation())
                    ->setHeadwords(new Headwords(
                        new Headword('headword 1.a.1'),
                        new Headword('ˈhɛdˌwɜːd 1.a.2'),
                    ))
                    ->setPronunciations(new Pronunciations(
                        new Pronunciation('pronunciation 1.a.1'),
                        new Pronunciation('prəˌnʌnsɪˈeɪʃən 1.a.2'),
                    ))
                    ->setDefinition(new Definition('definition 1.a'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.a.1'),
                        new Translation('trænsˈleɪʃən 1.a.2'),
                    )),
                (new Collocation())
                    ->setHeadwords(new Headwords(
                        new Headword('headword 1.b.1'),
                        new Headword('ˈhɛdˌwɜːd 1.b.2'),
                    ))
                    ->setPronunciations(new Pronunciations(
                        new Pronunciation('pronunciation 1.b.1'),
                        new Pronunciation('prəˌnʌnsɪˈeɪʃən 1.b.2'),
                    ))
                    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 1.b'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.b.1'),
                        new Translation('trænsˈleɪʃən 1.b.2'),
                    ))
            ))
            ->setSenses(new Senses(
                (new Sense())
                    ->setDefinition(new Definition('definition 1.1'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.1.1'),
                        new Translation('trænsˈleɪʃən 1.1.2'),
                    )),
                (new Sense())
                    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 1.2'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.2.1'),
                        new Translation('trænsˈleɪʃən 1.2.2'),
                    )),
            )),
        (new Sense())
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 2'))
            ->setTranslations(new Translations(
                new Translation('translation 2.1'),
                new Translation('trænsˈleɪʃən 2.2'),
            ))
    ))
    ->setDerivatives(new Derivatives(
        new Derivative('derivative 1'),
        new Derivative('derivative 2'),
    ))
;
