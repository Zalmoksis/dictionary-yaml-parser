<?php

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Collocation,
    Context,
    Definition,
    Headword,
    Pronunciation,
    Sense,
    Synonym,
    Translation,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Collocations,
    Headwords,
    Pronunciations,
    Synonyms,
    Translations,
};

return (new Sense())
    ->setContext(new Context('context'))
    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən'))
    ->setTranslations(new Translations(
        new Translation('translation 1'),
        new Translation('trænsˈleɪʃən 2'),
    ))
    ->setSynonyms(new Synonyms(
        new Synonym('synonym 1'),
        new Synonym('synonym 2'),
    ))
    ->setAntonyms(new Antonyms(
        new Antonym('antonym 1'),
        new Antonym('antonym 2'),
    ))
    ->setCollocations(new Collocations(
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword a.1'),
                new Headword('ˈhɛdˌwɜːd a.2')
            ))
            ->setPronunciations(new Pronunciations(
                new Pronunciation('pronunciation a.1'),
                new Pronunciation('prəˌnʌnsɪˈeɪʃən a.2')
            ))
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən a'))
            ->setTranslations(new Translations(
                new Translation('translation a.1'),
                new Translation('trænsˈleɪʃən a.2'),
            )),
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword b.1'),
                new Headword('ˈhɛdˌwɜːd b.2'),
            ))
            ->setTranslations(new Translations(
                new Translation('translation b.1'),
                new Translation('trænsˈleɪʃən b.2'),
            )),
    ))
;
