<?php

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Definition,
    Headword,
    Pronunciation,
    Sense,
    Translation,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Headwords,
    Pronunciations,
    Senses,
    Translations,
};

return (new Collocation())
    ->setHeadwords(new Headwords(
        new Headword('headword a.1'),
        new Headword('ˈhɛdˌwɜːd a.2')
    ))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation a.1'),
        new Pronunciation('prəˌnʌnsɪˈeɪʃən a.2'),
    ))
    ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən a'))
    ->setTranslations(new Translations(
        new Translation('translation a.1'),
        new Translation('trænsˈleɪʃən a.2'),
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setDefinition(new Definition('definition a.1'))
            ->setTranslations(new Translations(
                new Translation('translation a.1.1'),
                new Translation('trænsˈleɪʃən a.1.2'),
            )),
        (new Sense())
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən a.2'))
            ->setTranslations(new Translations(
                new Translation('translation a.2.1'),
                new Translation('trænsˈleɪʃən a.2.2'),
            ))
    ))
;
