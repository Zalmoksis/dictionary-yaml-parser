<?php

use Zalmoksis\Dictionary\Model\{
    Entry,
    Headword,
    Translation,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Headwords,
    Translations,
};

return (new Entry())
    ->setHeadwords(new Headwords(new Headword('headword 1')))
    ->setTranslations(new Translations(new Translation('translation 1')))
;
