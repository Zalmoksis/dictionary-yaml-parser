<?php

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Collocation,
    Definition,
    Derivative,
    Entry,
    Form,
    FormGroup,
    FormLabel,
    Headword,
    Pronunciation,
    Sense,
    Synonym,
    Translation
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Derivatives,
    FormNodes,
    Headwords,
    Pronunciations,
    Senses,
    Synonyms,
    Translations,
};

return (new Entry())
    ->setHeadwords(new Headwords(new Headword('ˈhɛdˌwɜːd')))
    ->setPronunciations(new Pronunciations(new Pronunciation('prəˌnʌnsɪˈeɪʃən')))
    ->setCategories(new Categories(new Category('ˈkætɪɡərɪ')))
    ->setFormNodes(new FormNodes(
        (new Form(new FormLabel('form label 1')))
            ->setHeadwords(new Headwords(new Headword('fɔːm 1'))),
        (new FormGroup(new FormLabel('form label 2')))
            ->setFormNodes(new FormNodes(
                (new Form(new FormLabel('fɔːm ˈleɪ bəl 2.1')))
                    ->setHeadwords(new Headwords(new Headword('fɔːm 2.1')))
            ))
    ))
    ->setTranslations(new Translations(new Translation('trænsˈleɪʃən')))
    ->setSynonyms(new Synonyms(new Synonym('synonym')))
    ->setAntonyms(new Antonyms(new Antonym('antonym')))
    /* TODO: some collocations should go here */
    ->setSenses(new Senses(
        (new Sense())
            ->setDefinition(new Definition('definition 1'))
            ->setTranslations(new Translations(new Translation('trænsˈleɪʃən 1'))),
        (new Sense())
            ->setDefinition(new Definition('ˌdɛfɪˈnɪʃən 2'))
            ->setTranslations(new Translations(new Translation('trænsˈleɪʃən 2'))),
    ))
    ->setDerivatives(new Derivatives(new Derivative('derivative')))
;
