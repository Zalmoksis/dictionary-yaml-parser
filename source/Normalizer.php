<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

interface Normalizer {
    function normalizeEntry(string $entryString): string;
    function normalizeSense(string $senseString): string;
    function normalizeCollocation(string $collocationString): string;
}
