<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

interface YamlParser {
    function parse($yaml);
    function emit($data): string;
}
