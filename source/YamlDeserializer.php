<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};
use Zalmoksis\Dictionary\Parser\ArrayParser\{ArrayDeserializer, ArrayNormalizer};

final class YamlDeserializer implements Deserializer {
    private YamlParser $yaml;
    private ArrayDeserializer $arrayDeserializer;
    private ?ArrayNormalizer $arrayNormalizer;

    function __construct(
        YamlParser $yaml,
        ArrayDeserializer $arrayDeserializer,
        ArrayNormalizer $arrayNormalizer = null
    ) {
        $this->yaml = $yaml;
        $this->arrayDeserializer = $arrayDeserializer;
        $this->arrayNormalizer = $arrayNormalizer;
    }

    function deserializeEntry(string $serializedEntry): Entry {
        $arrayEntry = $this->yaml->parse($serializedEntry);

        if ($this->arrayNormalizer) {
            $arrayEntry = $this->arrayNormalizer->normalizeEntry($arrayEntry);
        }

        return $this->arrayDeserializer->deserializeEntry($arrayEntry);
    }

    function deserializeSense(string $serializedSense): Sense {
        $arraySense = $this->yaml->parse($serializedSense);

        if ($this->arrayNormalizer) {
            $arraySense = $this->arrayNormalizer->normalizeSense($arraySense);
        }

        return $this->arrayDeserializer->deserializeSense($arraySense);
    }

    function deserializeCollocation(string $serializedCollocation): Collocation {
        $arrayCollocation = $this->yaml->parse($serializedCollocation);

        if ($this->arrayNormalizer) {
            $arrayCollocation = $this->arrayNormalizer->normalizeCollocation($arrayCollocation);
        }

        return $this->arrayDeserializer->deserializeCollocation($arrayCollocation);
    }
}
