<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Entry,
    Sense,
};

interface Deserializer {
    function deserializeEntry(string $serializedEntry): Entry;
    function deserializeSense(string $serializedSense): Sense;
    function deserializeCollocation(string $serializedCollocation): Collocation;
}
