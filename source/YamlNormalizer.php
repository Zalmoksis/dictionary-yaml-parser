<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Zalmoksis\Dictionary\Parser\ArrayParser\ArrayNormalizer;

final class YamlNormalizer implements Normalizer {
    private YamlParser $yaml;
    private ArrayNormalizer $arrayNormalizer;

    function __construct(
        YamlParser $yaml,
        ArrayNormalizer $arrayNormalizer
    ) {
        $this->yaml = $yaml;
        $this->arrayNormalizer = $arrayNormalizer;
    }

    function normalizeEntry(string $yamlEntry): string {
        return $this->yaml->emit(
            $this->arrayNormalizer->normalizeEntry(
                $this->yaml->parse($yamlEntry)
            )
        );
    }

    function normalizeSense(string $yamlSense): string {
        return $this->yaml->emit(
            $this->arrayNormalizer->normalizeEntry(
                $this->yaml->parse($yamlSense)
            )
        );
    }

    function normalizeCollocation(string $yamlCollocation): string {
        return $this->yaml->emit(
            $this->arrayNormalizer->normalizeEntry(
                $this->yaml->parse($yamlCollocation)
            )
        );
    }
}
