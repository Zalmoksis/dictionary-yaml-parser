<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use RuntimeException;

class YamlParsingException extends RuntimeException {
}
