<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Entry,
    Sense,
};
use Zalmoksis\Dictionary\Parser\ArrayParser\ArraySerializer;

final class YamlSerializer implements Serializer {
    private YamlParser $yaml;
    private ArraySerializer $arraySerializer;

    function __construct(
        YamlParser $yaml,
        ArraySerializer $arraySerializer
    ) {
        $this->yaml = $yaml;
        $this->arraySerializer = $arraySerializer;
    }

    function serializeEntry(Entry $entry): string {
        return $this->yaml->emit($this->arraySerializer->serializeEntry($entry));
    }

    function serializeSense(Sense $sense): string {
        return $this->yaml->emit($this->arraySerializer->serializeSense($sense));
    }

    function serializeCollocation(Collocation $collocation): string {
        return $this->yaml->emit($this->arraySerializer->serializeCollocation($collocation));
    }
}
