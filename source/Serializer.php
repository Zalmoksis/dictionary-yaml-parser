<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Entry,
    Sense,
};

interface Serializer {
    function serializeEntry(Entry $entry): string;
    function serializeSense(Sense $sense): string;
    function serializeCollocation(Collocation $collocation): string;
}
