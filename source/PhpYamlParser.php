<?php

namespace Zalmoksis\Dictionary\Parser\Yaml;

use Throwable;

final class PhpYamlParser implements YamlParser {

    function parse($yaml) {
        try {
            return yaml_parse($yaml);
        } catch (Throwable $exception) {
            throw new YamlParsingException($exception->getMessage(), 0, $exception);
        }
    }

    function emit($data): string {
        return $this->stripYamlSeparators(yaml_emit($data, YAML_UTF8_ENCODING));
    }

    private function stripYamlSeparators(string $yaml): string {
        return substr($yaml, 4, -4);
    }
}
