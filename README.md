# Dictionary: YAML Parser

This repository contains a solution allowing transformations between dictionary entry model
(described by `Zalmoksis\Dictionary`) and a YAML-based format.

The core components are:
- `YamlSerializer`
- `YamlDeserializer`
- `YamlNormalizer`
- `ArraySerializer`
- `ArrayDeserializer`
- `ArrayNormalizer`
