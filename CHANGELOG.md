# Changelog

All notable changes to this project will be documented in this file
in a format following  [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

The project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.15.5] — 2019-01-05
### Added
- Version range extension

## [0.15.4] — 2019-12-20
### Added
- Version range extension of `dictionary-model` and `dictionary-array-parser`

## [0.15.3] — 2019-12-12
### Added
- More tests

## [0.15.2] — 2019-12-11
### Added
- Testing lowest dependencies in Gitlab CI
### Changed
- Version range of `zalmoksis\dictionary-model` and `zalmoksis\dictionary-array-parser`

## [0.15.1] — 2019-12-03
### Fixed
- Coding standard dependency required only on dev

## [0.15.0] — 2019-12-03
### Added
- `Normalizer` interface; applied to `YamlNormalizer`
### Changed
- Required version of PHP increased to 7.4
- Strong typing of properties
- Classes are now final
- Gitlab CI integration
- Improved PHP Unit configuration

## [0.14.1]
### Changed
- Version range increase of [zalmoksis\dictionary-array-parser](https://gitlab.com/Zalmoksis/dictionary-array-parser)
  to 0.3.*

## [0.14.0]
### Changed
- Switching to zalmoksis\dictionary-array-parser
### Removed
- Array parsing classes: `ArraySerializer`, `ArrayDeserializer`, `ArrayNormalizer`,
  `DefaultArraySerializer`, `DefaultArrayDeserializer`, `DefaultArrayNormalizer`

## [0.13.0]
### Added
- `YamlNormalizer` allowing normalization of YAML without creating model objects
- `PhpYamlParser` isolating PHP Yaml extension, wrapping warnings and enforcing common configuration
### Changed
- Removal of "Interface" from interfaces
  - `StringSerializerInterface` changed to `Serializer`
  - `StringDeserializerInterface` changed to `Deserializer`
  - `ArraySerializerInterface` changed to `ArraySerializer`
  - `ArrayDerializerInterface` changed to `ArrayDerializer`
  - `ArrayNormalizerInterface` changed to `ArrayNormalizer`
- All predefined instances of `Array*` interfaces are now prefixed with `Default`
  - `ArraySerializerInterface` changed to `DefaultArraySerializer`
  - `ArrayDeserializerInterface` changed to `DefaultArrayDeserializer`
  - `ArrayNormalizerInterface` changed to `DefaultArrayNormalizer`
- `YamlSerializer` and `YamlDeserializer` require now an instance of `YamlParser`
### Removed
- serialization of simple values

## [0.12.1]
### Fixed
- Fixed `YamlSerializer::serializeCollocation` not working

## [0.12.0]
### Added
- Array normalization allowing key shorthands in the deserializer
  and in result shorthands of YAML keys.
- All the interfaces include operations on collocations now.

## [0.11.0] - 2019-01-24
### Changed
- Stripping document separators from YAML output

## [0.10.0] - 2019-01-19
### Added
- Homograph index support

## [0.9.3] - 2019-01-18
### Added
- MIT Licence

## [0.9.2] - 2019-01-18
### Added
- This changelog

## [0.9.1] - 2019-01-18
### Changed
- Extended version range of [zalmoksis\dictionary](https://gitlab.com/Zalmoksis/dictionary) up to current 0.11.*

## [0.9.0] - 2019-01-17
### Added
- Support for nested senses
